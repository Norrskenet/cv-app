//
//  ExperienceViewController.swift
//  cv-app
//
//  Created by Rasmus Andersparr on 2019-12-07.
//  Copyright © 2019 Rasmus Andersparr. All rights reserved.
//

import UIKit

class ExperienceViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView?
    
    struct dataWork{
        let work: String
        let years: String
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    static let work: [dataWork] = [
        .init(work: "Work 1", years: "2012-2014"),
        .init(work: "Work 2", years: "2015-2017"),
        .init(work: "Work 3", years: "2017-2018"),
        .init(work: "Work 4", years: "2018-2020")
    ]
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class WorkCell: UITableViewCell{
    @IBOutlet private weak var workLabel: UILabel!
    @IBOutlet private weak var yearLabel: UILabel!
    
    var work:String?{
        didSet{ workLabel.text = work?.workFormatted ?? ""}
    }
    var year:String?{
        didSet{ yearLabel.text = year?.yearFormatted ?? ""}
    }
    
}



extension String{
    var workFormatted: String{
        return "- " + self
    }
    var yearFormatted: String{
        return "'" + self + "'"
    }
}
